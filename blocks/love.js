Blockly.Blocks['love_draw'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Draw");
    this.appendStatementInput("ACTIONS")
        .setCheck(null);
    this.setColour(65);
    this.setTooltip('What to draw when the game is run.');
  }
};

Blockly.Blocks['love_update'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Update");
    this.appendStatementInput("ACTIONS")
        .setCheck(null);
    this.setColour(65);
    this.setTooltip('Update the game state.');
  }
};

Blockly.Blocks['love_ignore'] = {
  init: function() {
    this.appendDummyInput()
        .setAlign(Blockly.ALIGN_CENTRE)
        .appendField("Ignore");
    this.appendValueInput("VALUE")
        .setCheck(null);
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(65);
  }
};
