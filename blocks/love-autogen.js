(function() {

	var typeLuaToJS = function(luaType) {
		if(luaType == "number") {
			luaType = "Number";
		}  else if(luaType == "string") {
			luaType = "String";
		} else if(luaType == "boolean") {
			luaType = "Boolean";
		} else if(luaType == "table") {
			luaType = "dict";
		} else {
			return {known: false, type: luaType};
		}
		return {known: true, type: luaType};
	}

	var addEnumToDraw = function(blocName, enumName, name, drawer, colour, values) {
		var newTag = document.createElement('block');
		newTag.setAttribute('type', blocName);
		drawer.appendChild(newTag);

		Blockly.Blocks[blocName] = {
			init: function() {
				this.appendDummyInput()
						.setAlign(Blockly.ALIGN_CENTRE)
						.appendField(name);
				this.setOutput(true, enumName);
				this.setInputsInline(true);
				this.setColour(colour);

				var opts = [];
				for(var opt of values) {
					opts.push([opt, opt]);
				}
				this.appendDummyInput()
					.appendField(new Blockly.FieldDropdown(opts), "IN");
			}
		};

		Blockly.Lua[blocName] = function(block) {
			var value = block.getFieldValue("IN");
			var code = "\"" + value + "\"";
			// Return that string
			return [code, Blockly.Lua.ORDER_ATOMIC];
		};
	}

	var addBlockFwdToDraw = function(blocName, name, args, drawer, colour, codeName, outType) {
		var newTag = document.createElement('block');
		newTag.setAttribute('type', blocName);

		for(var arg in args) {
			var type = args[arg];

			var valTag = document.createElement('value');
			valTag.setAttribute('name', arg);
			if(type == "number") {
				var shadow = document.createElement('shadow');
				shadow.setAttribute('type', 'math_number');
					var field = document.createElement('field');
					field.setAttribute('name', 'NUM');
					field.innerHTML = '0';
					shadow.appendChild(field);
				valTag.appendChild(shadow);
			}  else if(type == "string") {
			} else if(type == "boolean") {
			} else if(type == "table") {
			} else {
				var shadow = document.createElement('shadow');
				shadow.setAttribute('type', "love_autogen_enums_" + type);
					// var field = document.createElement('field');
					// field.setAttribute('name', 'IN');
					// field.innerHTML = '0';
					// shadow.appendChild(field);
				valTag.appendChild(shadow);
			}
			newTag.appendChild(valTag);
		}

		drawer.appendChild(newTag);

		Blockly.Blocks[blocName] = {
			init: function() {
				this.appendDummyInput()
						.setAlign(Blockly.ALIGN_CENTRE)
						.appendField(name);
				if(outType == null) {
					this.setPreviousStatement(true, null);
					this.setNextStatement(true, null);
				} else {
					this.setOutput(true, typeLuaToJS(outType).type);
				}
				this.setInputsInline(false);
				this.setColour(colour);
				for(var arg in args) {
					var type = args[arg];
					type = typeLuaToJS(type);
					// if(type.known) {
					// 	type = null;
					// } else {
						type = type.type;
					// }
					this.appendValueInput(arg)
							.appendField(arg)
							.setCheck(type);
				}
			}
		};

		Blockly.Lua[blocName] = function(block) {
			var argList = [];
			for(var arg in args) {
				var type = args[arg];
				if(typeof type == "object") {
					argList.push("\"" + block.getFieldValue(arg) + "\"");
				} else {
					argList.push(Blockly.Lua.valueToCode(block, arg, Blockly.Lua.ORDER_ATOMIC));
				}
			}
		  // Assemble Lua into code.
		  var code = codeName + '(';
			var isFirst = true;
			for(var arg of argList) {
				if(!isFirst) {
					code += ", ";
				}
				code += arg;
				isFirst = false;
			}
			code += ')';
			if(outType == null) {
			  return code + '\n';
			} else {
				return [code, Blockly.Lua.ORDER_ATOMIC];
			}
		};
	}

	var addAllToDrawer = function(input, prefix, drawerId, funcDomain, colour) {
		var drawerRealId = "blocklydraw-" + drawerId;
		var drawer = document.getElementById(drawerRealId);
		if(drawer == null) {
			drawer = document.createElement('category');
			drawer.setAttribute('name', "{" + drawerId + "}");
			drawer.setAttribute('id', drawerRealId);
			drawer.setAttribute('colour', colour);
			document.getElementById("toolbox").appendChild(drawer);
		}
		var enums = input.enums;
		for(var name in enums) {
			var blocName = "love_autogen_enums_" + name;
			addEnumToDraw(blocName,
												name,
												name.charAt(0).toUpperCase() + name.slice(1),
												drawer,
												colour,
												enums[name]
											);
		}
		var funcs = input.functions;
		for(var name in funcs) {
			var forms = funcs[name];
			var formID = 0;
			for(var data of forms) {
				var outType = data.ret;
				var args = data.args;

				formID++;
				var blocName = prefix + name + "_" + formID;
				addBlockFwdToDraw(blocName,
													name.charAt(0).toUpperCase() + name.slice(1),
													args,
													drawer,
													colour,
													funcDomain + name,
													outType);
			}
		}
	}

	addAllToDrawer(
		Blockly.Love2d.graphics,
		"love_autogen_graphics_",
		"catLoveAdvDraw",
		"love.graphics.",
		120);
	addAllToDrawer(
		Blockly.Love2d.graphics_simple,
		"love_autogen_graphics_simple_",
		"catLove",
		"love.graphics.",
		120); // 65
	addAllToDrawer(
		Blockly.Love2d.mouse,
		"love_autogen_mouse_",
		"catLoveMouse",
		"love.mouse.",
		290);
	addAllToDrawer(
		Blockly.Love2d.keyboard,
		"love_autogen_keyboard_",
		"catLoveKeyboard",
		"love.keyboard.",
		320);
})();
