new QWebChannel(qt.webChannelTransport, function (channel) {
	// now you retrieve your object
	IDE._io = channel.objects.IDE_IO;
});

IDE.run = function(lua) {
	// Invoke a method:
	IDE._io.runLua(lua, function(returnValue) {
		// This callback will be invoked when myMethod has a return value. Keep in mind that
		// the communication is asynchronous, hence the need for this callback.
		console.log(returnValue);
	});
}
