/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating Lua for dictionary blocks.
 * @author acbart@vt.edu (Austin Cory Bart)
 */
'use strict';

goog.provide('Blockly.Lua.dicts');

goog.require('Blockly.Lua');

Blockly.Lua['dict_get'] = function(block) {
  var dict = Blockly.Lua.valueToCode(block, 'DICT',
      Blockly.Lua.ORDER_MEMBER) || '_';
  var value = Blockly.Lua.valueToCode(block, 'ITEM',
      Blockly.Lua.ORDER_NONE) || '_';
  var code = dict + '[' + value + ']';
  return [code, Blockly.Lua.ORDER_ATOMIC];
};


Blockly.Lua['dict_get_literal'] = function(block) {
  var dict = Blockly.Lua.valueToCode(block, 'DICT',
      Blockly.Lua.ORDER_MEMBER) || '_';
  var value = Blockly.Lua.quote_(block.getFieldValue('ITEM'));
  var code = dict + '[' + value + ']';
  return [code, Blockly.Lua.ORDER_ATOMIC];
};


Blockly.Lua['dicts_create_with'] = function(block) {
    var value_keys = Blockly.Lua.valueToCode(block, 'keys', Blockly.   Lua.ORDER_ATOMIC);
    // TODO: Assemble Lua into code variable.
    var code = new Array(block.itemCount_);

    for (var n = 0; n < block.itemCount_; n++) {
        var key = Blockly.Lua.quote_(block.getFieldValue('KEY' + n));
        var value = Blockly.Lua.valueToCode(block, 'VALUE' + n,
                Blockly.Lua.ORDER_NONE) || '_';
        code[n] = "[" + key + "]: "+ value;
    }
    code = '{' + code.join(', ') + '}';
    return [code, Blockly.Lua.ORDER_ATOMIC];
};

Blockly.Lua['dict_keys'] = function(block) {
  var dict = Blockly.Lua.valueToCode(block, 'DICT',
      Blockly.Lua.ORDER_MEMBER) || '___';
  var code = dict + '.keys()';
  return [code, Blockly.Lua.ORDER_ATOMIC];
};
