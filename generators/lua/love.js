Blockly.Love2d = {};

Blockly.Lua.addReservedWords(
    // Love2d and LoveBlockly
		'love', 'loveblockly', '_'
);

Blockly.Lua.getBoilerplateCode = function() {
	return ""
	+	"\n" + "loveblockly = {"
	+	"\n" + "  draws = {},"
	+	"\n" + "  updates = {}"
	+	"\n" + "}"
	+	"\n" + ""
	+	"\n" + "function love.draw()"
	+	"\n" + "  for _, draw in ipairs(loveblockly.draws) do draw() end"
	+	"\n" + "end"
	+	"\n" + "function love.update()"
	+	"\n" + "  for _, update in ipairs(loveblockly.updates) do update() end"
	+	"\n" + "end"
	+	"\n" + "\n";
}

Blockly.Lua['love_draw'] = function(block) {
  var statements_actions = Blockly.Lua.statementToCode(block, 'ACTIONS');
  // Assemble Lua into code.
  var code = 'table.insert(loveblockly.draws, function()\n' + statements_actions + 'end)\n';
  return code;
};

Blockly.Lua['love_update'] = function(block) {
  var statements_actions = Blockly.Lua.statementToCode(block, 'ACTIONS');
  // Assemble Lua into code.
  var code = 'table.insert(loveblockly.updates, function()\n' + statements_actions + 'end)\n';
  return code;
};


Blockly.Lua['love_ignore'] = function(block) {
  var value_value = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_ATOMIC);
  // TODO: Assemble Lua into code variable.
  var code = '_ = ' + value_value;
  return code;
};
