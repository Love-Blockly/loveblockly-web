Blockly.Love2d.graphics = {
"enums":
{
  "AlignMode": ["center", "left", "right"],
  "ArcType": ["pie", "open", "closed"],
  "AreaSpreadDistribution": ["uniform", "normal", "none"],
  "BlendAlphaMode": ["alphamultiply", "premultiplied"],
  "BlendMode": ["alpha", "replace", "screen", "add", "subtract", "multiply", "lighten", "darken"],
  "CanvasFormat": ["normal", "hdr", "rgba8", "rgba4", "rgb5a1", "rgb565", "rgb10a2", "rgba16f", "rgba32f", "rg11b10f", "srgb", "r8", "rg8", "r16f", "rg16f", "r32f", "rg32f"],
  "CompareMode": ["equal", "notequal", "less", "lequal", "gequal", "greater"],
  "DrawMode": ["fill", "line"],
  "FilterMode": ["linear", "nearest"],
  "GraphicsFeature": ["clampzero", "lighten", "multicanvasformats"],
  "GraphicsLimit": ["pointsize", "texturesize", "multicanvas", "canvasmsaa"],
  "LineJoin": ["miter", "bevel", "none"],
  "LineStyle": ["rough", "smooth"],
  "MeshDrawMode": ["fan", "strip", "triangles", "points"],
  "ParticleInsertMode": ["top", "bottom", "random"],
  "PointStyle": ["rough", "smooth"],
  "SpriteBatchUsage": ["dynamic", "static", "stream"],
  "StackType": ["transform", "all"],
  "StencilAction": ["replace", "increment", "decrement", "incrementwrap", "decrementwrap", "invert"],
  "WrapMode": ["clamp", "repeat", "mirroredrepeat", "clampzero"],
}
, "functions": {

"arc": [
  {"ret": null, "args": {"drawmode": "DrawMode", "x": "number", "y": "number", "radius": "number", "angle1": "number", "angle2": "number", "segments": "number"}}
  ,{"ret": null, "args": {"drawmode": "DrawMode", "arctype": "ArcType", "x": "number", "y": "number", "radius": "number", "angle1": "number", "angle2": "number", "segments": "number"}}
],
"circle": [
  {"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "radius": "number", "segments": "number"}}
],
"clear": [
  {"ret": null, "args": {}}
  ,{"ret": null, "args": {"r": "number", "g": "number", "b": "number", "a": "number"}}
  ,{"ret": null, "args": {"color": "table", "...": "table"}}
],
"discard": [
  {"ret": null, "args": {"discardcolor": "boolean", "discardstencil": "boolean"}}
  ,{"ret": null, "args": {"discardcolors": "table", "discardstencil": "boolean"}}
],
"draw": [
  {"ret": null, "args": {"drawable": "Drawable", "x": "number", "y": "number", "r": "number", "sx": "number", "sy": "number", "ox": "number", "oy": "number", "kx": "number", "ky": "number"}}
  ,{"ret": null, "args": {"texture": "Texture", "quad": "Quad", "x": "number", "y": "number", "r": "number", "sx": "number", "sy": "number", "ox": "number", "oy": "number", "kx": "number", "ky": "number"}}
],
"ellipse": [
  {"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "radiusx": "number", "radiusy": "number"}}
  ,{"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "radiusx": "number", "radiusy": "number", "segments": "number"}}
],
"getBackgroundColor": [
  {"ret": "number", "args": {}}
],
"getBlendMode": [
  {"ret": "BlendMode", "args": {}}
],
"getCanvas": [
  {"ret": "Canvas", "args": {}}
],
"getCanvasFormats": [
  {"ret": "table", "args": {}}
],
"getColor": [
  {"ret": "number", "args": {}}
],
"getColorMask": [
  {"ret": "boolean", "args": {}}
],
"getCompressedImageFormats": [
  {"ret": "table", "args": {}}
],
"getDefaultFilter": [
  {"ret": "FilterMode", "args": {}}
],
"getDimensions": [
  {"ret": "number", "args": {}}
],
"getFont": [
  {"ret": "Font", "args": {}}
],
"getHeight": [
  {"ret": "number", "args": {}}
],
"getLineJoin": [
  {"ret": "LineJoin", "args": {}}
],
"getLineStyle": [
  {"ret": "LineStyle", "args": {}}
],
"getLineWidth": [
  {"ret": "number", "args": {}}
],
"getShader": [
  {"ret": "Shader", "args": {}}
],
"getStats": [
  {"ret": "number", "args": {}}
],
"getStencilTest": [
  {"ret": "boolean", "args": {}}
],
"getSupported": [
  {"ret": "table", "args": {}}
],
"getSystemLimits": [
  {"ret": "table", "args": {}}
],
"getPointSize": [
  {"ret": "number", "args": {}}
],
"getRendererInfo": [
  {"ret": "string", "args": {}}
],
"getScissor": [
  {"ret": "number", "args": {}}
],
"getWidth": [
  {"ret": "number", "args": {}}
],
"intersectScissor": [
  {"ret": null, "args": {"x": "number", "y": "number", "width": "number", "height": "number"}}
  ,{"ret": null, "args": {}}
],
"isGammaCorrect": [
  {"ret": "boolean", "args": {}}
],
"isWireframe": [
  {"ret": "boolean", "args": {}}
],
"line": [
  {"ret": null, "args": {"x1": "number", "y1": "number", "x2": "number", "y2": "number", "...": "number"}}
  ,{"ret": null, "args": {"points": "table"}}
],
"newCanvas": [
  {"ret": "Canvas", "args": {"width": "number", "height": "number", "format": "CanvasFormat", "msaa": "number"}}
],
"newFont": [
  {"ret": "Font", "args": {"filename": "string", "size": "number"}}
  ,{"ret": "Font", "args": {"file": "File", "size": "number"}}
  ,{"ret": "Font", "args": {"filedata": "FileData", "size": "number"}}
  ,{"ret": "Font", "args": {"size": "number"}}
],
"newMesh": [
  {"ret": "Mesh", "args": {"vertices": "table", "mode": "MeshDrawMode", "usage": "SpriteBatchUsage"}}
  ,{"ret": "Mesh", "args": {"vertexcount": "number", "mode": "MeshDrawMode", "usage": "SpriteBatchUsage"}}
  ,{"ret": "Mesh", "args": {"vertexformat": "table", "vertices": "table", "mode": "MeshDrawMode", "usage": "SpriteBatchUsage"}}
  ,{"ret": "Mesh", "args": {"vertexformat": "table", "vertexcount": "number", "mode": "MeshDrawMode", "usage": "SpriteBatchUsage"}}
],
"newImage": [
  {"ret": "Image", "args": {"filename": "string"}}
  ,{"ret": "Image", "args": {"imageData": "ImageData"}}
  ,{"ret": "Image", "args": {"compressedImageData": "CompressedImageData"}}
  ,{"ret": "Image", "args": {"filename": "string", "flags": "table"}}
],
"newImageFont": [
  {"ret": "Font", "args": {"filename": "string", "glyphs": "string", "extraspacing": "number"}}
],
"newParticleSystem": [
  {"ret": "ParticleSystem", "args": {"texture": "Texture", "buffer": "number"}}
],
"newShader": [
  {"ret": "Shader", "args": {"code": "string"}}
  ,{"ret": "Shader", "args": {"pixelcode": "string", "vertexcode": "string"}}
],
"newText": [
  {"ret": "Text", "args": {"font": "Font", "textstring": "string"}}
],
"newQuad": [
  {"ret": "Quad", "args": {"x": "number", "y": "number", "width": "number", "height": "number", "sw": "number", "sh": "number"}}
],
"newScreenshot": [
  {"ret": "ImageData", "args": {"copyAlpha": "boolean"}}
],
"newSpriteBatch": [
  {"ret": "SpriteBatch", "args": {"texture": "Texture", "maxsprites": "number", "usage": "SpriteBatchUsage"}}
],
"newVideo": [
  {"ret": "Video", "args": {"filename": "string", "loadaudio": "boolean"}}
  ,{"ret": "Video", "args": {"videostream": "VideoStream", "loadaudio": "boolean"}}
],
"origin": [
  {"ret": null, "args": {}}
],
"points": [
  {"ret": null, "args": {"x": "number", "y": "number", "...": "number"}}
  ,{"ret": null, "args": {"points": "table"}}
  ,{"ret": null, "args": {"points": "table"}}
],
"polygon": [
  {"ret": null, "args": {"mode": "DrawMode", "...": "number"}}
  ,{"ret": null, "args": {"mode": "DrawMode", "vertices": "table"}}
],
"pop": [
  {"ret": null, "args": {}}
],
"present": [
  {"ret": null, "args": {}}
],
"print": [
  {"ret": null, "args": {"text": "string", "x": "number", "y": "number", "r": "number", "sx": "number", "sy": "number", "ox": "number", "oy": "number", "kx": "number", "ky": "number"}}
  ,{"ret": null, "args": {"coloredtext": "table", "x": "number", "y": "number", "angle": "number", "sx": "number", "sy": "number", "ox": "number", "oy": "number", "kx": "number", "ky": "number"}}
],
"printf": [
  {"ret": null, "args": {"text": "string", "x": "number", "y": "number", "limit": "number", "align": "AlignMode", "r": "number", "sx": "number", "sy": "number", "ox": "number", "oy": "number", "kx": "number", "ky": "number"}}
  ,{"ret": null, "args": {"coloredtext": "table", "x": "number", "y": "number", "wraplimit": "number", "align": "AlignMode", "angle": "number", "sx": "number", "sy": "number", "ox": "number", "oy": "number", "kx": "number", "ky": "number"}}
],
"push": [
  {"ret": null, "args": {"stack": "StackType"}}
],
"rectangle": [
  {"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "width": "number", "height": "number"}}
  ,{"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "width": "number", "height": "number", "rx": "number", "ry": "number", "segments": "number"}}
],
"reset": [
  {"ret": null, "args": {}}
],
"rotate": [
  {"ret": null, "args": {"angle": "number"}}
],
"scale": [
  {"ret": null, "args": {"sx": "number", "sy": "number"}}
],
"setBackgroundColor": [
  {"ret": null, "args": {"r": "number", "g": "number", "b": "number"}}
	,{"ret": null, "args": {"r": "number", "g": "number", "b": "number", "a": "number"}}
  ,{"ret": null, "args": {"rgba": "table"}}
],
"setBlendMode": [
  {"ret": null, "args": {"mode": "BlendMode"}}
  ,{"ret": null, "args": {"mode": "BlendMode", "alphamode": "BlendAlphaMode"}}
],
"setCanvas": [
  {"ret": null, "args": {}}
  ,{"ret": null, "args": {"canvas": "Canvas", "...": "Canvas"}}
],
"setColor": [
  {"ret": null, "args": {"red": "number", "green": "number", "blue": "number", "alpha": "number"}}
  ,{"ret": null, "args": {"rgba": "table"}}
],
"setColorMask": [
  {"ret": null, "args": {}}
  ,{"ret": null, "args": {"red": "boolean", "green": "boolean", "blue": "boolean", "alpha": "boolean"}}
],
"setDefaultFilter": [
  {"ret": null, "args": {"min": "FilterMode", "mag": "FilterMode", "anisotropy": "number"}}
],
"setFont": [
  {"ret": null, "args": {"font": "Font"}}
],
"setLineJoin": [
  {"ret": null, "args": {"join": "LineJoin"}}
],
"setLineStyle": [
  {"ret": null, "args": {"style": "LineStyle"}}
],
"setLineWidth": [
  {"ret": null, "args": {"width": "number"}}
],
"setNewFont": [
  {"ret": "Font", "args": {"filename": "string", "size": "number"}}
  ,{"ret": "Font", "args": {"file": "File", "size": "number"}}
  ,{"ret": "Font", "args": {"data": "Data", "size": "number"}}
],
"setShader": [
  {"ret": null, "args": {}}
  ,{"ret": null, "args": {"shader": "Shader"}}
],
"setPointSize": [
  {"ret": null, "args": {"size": "number"}}
],
"setScissor": [
  {"ret": null, "args": {"x": "number", "y": "number", "width": "number", "height": "number"}}
  ,{"ret": null, "args": {}}
],
"setStencilTest": [
  {"ret": null, "args": {"comparemode": "CompareMode", "comparevalue": "number"}}
  ,{"ret": null, "args": {}}
],
"setWireframe": [
  {"ret": null, "args": {"enable": "boolean"}}
],
"shear": [
  {"ret": null, "args": {"kx": "number", "ky": "number"}}
],
"stencil": [
  {"ret": null, "args": {"stencilfunction": "function", "action": "StencilAction", "value": "number", "keepvalues": "boolean"}}
],
"translate": [
  {"ret": null, "args": {"dx": "number", "dy": "number"}}
],
}
};
