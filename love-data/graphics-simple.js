Blockly.Love2d.graphics_simple = {
"enums":
{
}
, "functions": {
"ellipse": [
  {"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "radiusx": "number", "radiusy": "number"}}
],
"line": [
  {"ret": null, "args": {"x1": "number", "y1": "number", "x2": "number", "y2": "number"}}
],
"print": [
  {"ret": null, "args": {"text": "string", "x": "number", "y": "number"}}
],
"rectangle": [
  {"ret": null, "args": {"mode": "DrawMode", "x": "number", "y": "number", "width": "number", "height": "number"}}
],
"setBackgroundColor": [
  {"ret": null, "args": {"r": "number", "g": "number", "b": "number"}}
],
"setColor": [
  {"ret": null, "args": {"red": "number", "green": "number", "blue": "number", "alpha": "number"}}
],
"clear": [
  {"ret": null, "args": {}}
]
}
};
