Blockly.Love2d.mouse = {
"enums":
{
  "CursorType": ["image", "arrow", "ibeam", "wait", "waitarrow", "crosshair", "sizenwse", "sizenesw", "sizewe", "sizens", "sizeall", "no", "hand"],
}
, "functions": {

"getCursor": [
  {"ret": "Cursor", "args": {}}
],
"getPosition": [
  {"ret": "number", "args": {}}
],
"getRelativeMode": [
  {"ret": "boolean", "args": {}}
],
"getSystemCursor": [
  {"ret": "Cursor", "args": {"ctype": "CursorType"}}
],
"getX": [
  {"ret": "number", "args": {}}
],
"getY": [
  {"ret": "number", "args": {}}
],
"hasCursor": [
  {"ret": "boolean", "args": {}}
],
"isDown": [
  {"ret": "boolean", "args": {"button": "number"}}
],
"isGrabbed": [
  {"ret": "boolean", "args": {}}
],
"isVisible": [
  {"ret": "boolean", "args": {}}
],
"newCursor": [
  {"ret": "Cursor", "args": {"imageData": "ImageData", "hotx": "number", "hoty": "number"}}
  ,{"ret": "Cursor", "args": {"filepath": "string", "hotx": "number", "hoty": "number"}}
  ,{"ret": "Cursor", "args": {"fileData": "FileData", "hotx": "number", "hoty": "number"}}
],
"setCursor": [
  {"ret": null, "args": {}}
  ,{"ret": null, "args": {"cursor": "Cursor"}}
],
"setGrabbed": [
  {"ret": null, "args": {"grab": "boolean"}}
],
"setPosition": [
  {"ret": null, "args": {"x": "number", "y": "number"}}
],
"setRelativeMode": [
  {"ret": null, "args": {"enable": "boolean"}}
],
"setVisible": [
  {"ret": null, "args": {"visible": "boolean"}}
],
"setX": [
  {"ret": null, "args": {"x": "number"}}
],
"setY": [
  {"ret": null, "args": {"y": "number"}}
],
}
};
