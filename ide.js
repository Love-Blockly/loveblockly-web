/**
 * @fileoverview IDE intergration.
 * @author znix@znix.xyz (Campbell Suter)
 */
'use strict';

var IDE = (function() {
	if (typeof IDE == 'undefined') {
		return {
			run: function(){
				alert("You are not in the IDE!");
			},
		};
	} else {
		// Load the WebChannel API
		document.write('<script src="qrc:///qtwebchannel/qwebchannel.js"></script>\n');
		// Load the IDE Implementation
		document.write('<script src="ide-impl.js"></script>\n');

		// Don't change IDE.
		return IDE;
	}
})();
